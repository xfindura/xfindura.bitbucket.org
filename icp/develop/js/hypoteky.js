var unicredit = {
    "id": 1,
    "banka": "UniCredit",
    "urok": 1.99,
    "fixacia": 5,
    "splatka": 254.02,
    "ltv": 90,
    "image": "img/unicredit.png",
    "info": '+ možnosť jednej mimoriadnej splátky vo výške 20% zostatku úveru kedykoľvek</br></br>+ možnosť použiť aj na splatenie bezúčelového spotrebného úveru, alebo na úhradu faktúr za tovary alebo služby spojené s bývaním do výšky 40% zo sumy úveru v prípade, ak výška úveru nepresahuje 80% LTV</br></br>+ možné transparetné úročenie'
}

var mbank = {
    "id": 2,
    "banka": "mBank",
    "urok": 2.04,
    "fixacia": 5,
    "splatka": 255.48,
    "ltv": 80,
    "image": "img/mbank.png",

    "info": '+ garantovaná výška úrokovej sadzby pre každého</br></br>+ bez poplatku za poskytnutie</br></br>+ čiastočné mimoriadne splátky do výšky 20% istiny zadarmo</br></br>+ založenie a vedenie úverového aj osobného účtu zadarmo'
}

var fio = {
    "id": 3,
    "banka": "FIO banka",
    "urok": 2.09,
    "fixacia": 5,
    "splatka": 256.66,
    "ltv": 85,
    "image": "img/fiobanka.png",
    "info": '+ bez poplatku za poskytnutie úveru</br></br>+ bezplatné vedenie bežného účtu</br></br>+ mimoriadna splátka 20% ročne bez poplatku</br></br>+ čerpanie do výšky 20% úveru bez dokladov</br></br>+ možnosť zhodnotiť voľné zdroje určené na splátku hypotéky na Hyposporiacom konte'
}

var slsp = {
    "id": 4,
    "banka": "SLSP",
    "urok": 2.20,
    "fixacia": 5,
    "splatka": 260.20,
    "ltv": 100,
    "image": "img/slsp.png",
    "info": '+ podnikatelia môžu získať úver na súkromné účely aj na základe posúdenia ich dosiahnutého percenta z tržieb</br></br>+ pri úveroch nad 70% hodnoty nehnuteľnosti stačí zdokladovať 80% výšky úveru</br></br>+ vybavenie a zaplatenie znaleckého posuku k nehnuteľnosti a vkladu záložného práva do katastra bankou</br></br>+ možnosť splatiť 20% intiny ročne bez poplatku'
}

var ponuky = [unicredit, mbank, fio, slsp];

var sort_urok = [ 1, 2, 3, 4 ];

var sort_ltv = [ 2, 3, 1, 4 ];

var sort_splatka = [ 1, 2, 3, 4 ];

var sort_fixacia = [ 1, 2, 3, 4 ];

function getHypoCard(h) {
    var hypo_card = '<div class="hypoteka">';
    hypo_card += '<div class="hypoID" data-value="'+h["id"]+'" style="display:none;"></div>';
    hypo_card += '<div class="hypo-head">';
    hypo_card += '<div class="bankImage"><img src="'+h["image"]+'"></div>';
    hypo_card += '<div class="hypo-buttons">';
    hypo_card += '<a href="poziadaj.html?id='+h["id"]+'"><div class="poziadajButton myButton">Požiadaj</div></a>';
    hypo_card += '<div class="addFavButton myButton circle">+</div>';
    hypo_card += '<div class="openButton"><div class="arrow_down"></div></div>';
    hypo_card += '</div>';
    hypo_card += '</div><div class="hypo-content"><table>';
    hypo_card += '<tr><td>Úrok</td>';
    hypo_card += '<td>'+h["urok"]+'%</td></tr>';
    hypo_card += '<tr><td>Fixácia</td>';
    hypo_card += '<td>'+h["fixacia"]+'%</td></tr>';
    hypo_card += '<tr><td>Mesačná splátka</td>';
    hypo_card += '<td>'+h["splatka"]+'%</td></tr>';
    hypo_card += '<tr><td>Max LTV</td>';
    hypo_card += '<td>'+h["ltv"]+'%</td></tr>';
    hypo_card += '</table><div class="hypo-slider">'+h["info"]+'</div></div></div>';
    return hypo_card;
}

function getHypoRow(h) {
    var hypo_row = '<tr><td>'+h["image"]+'</td><td>'+h["urok"]+'</td><td>'+h["fixacia"]+'</td><td>'+h["splatka"]+'</td><td>'+h["ltv"]+'</td>';
    hypo_row += '<td><a href="'+window.location.href+'"><div class="removeFavButton">X</div></a><div class="hypoID" data-value="'+h["id"]+'" style="display:none;"></div></td></tr>';
    return hypo_row;
}