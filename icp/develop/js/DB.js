var DB = function() {

	this.unicredit = {
        "id": 0,
        "banka": "UniCredit",
        "urok": 1.99,
        "fixacia": 5,
        "splatka": 254.02,
		"pa": 3034.94,
        "ltv": 90,
        "image": "img/unicredit.png",
        "info": '+ možnosť jednej mimoriadnej splátky vo výške 20% zostatku úveru kedykoľvek</br></br>+ možnosť použiť aj na splatenie bezúčelového spotrebného úveru, alebo na úhradu faktúr za tovary alebo služby spojené s bývaním do výšky 40% zo sumy úveru v prípade, ak výška úveru nepresahuje 80% LTV</br></br>+ možné transparetné úročenie'
    }

    this.mbank = {
        "id": 1,
        "banka": "mBank",
        "urok": 2.04,
        "fixacia": 5,
        "splatka": 255.48,
		"pa": 3065.79,
        "ltv": 80,
        "image": "img/mbank.png",

        "info": '+ garantovaná výška úrokovej sadzby pre každého</br></br>+ bez poplatku za poskytnutie</br></br>+ čiastočné mimoriadne splátky do výšky 20% istiny zadarmo</br></br>+ založenie a vedenie úverového aj osobného účtu zadarmo'
    }

    this.fio = {
        "id": 2,
        "banka": "FIO banka",
        "urok": 2.09,
        "fixacia": 5,
        "splatka": 256.66,
		"pa": 3079.87,
        "ltv": 85,
        "image": "img/fiobanka.png",
        "info": '+ bez poplatku za poskytnutie úveru</br></br>+ bezplatné vedenie bežného účtu</br></br>+ mimoriadna splátka 20% ročne bez poplatku</br></br>+ čerpanie do výšky 20% úveru bez dokladov</br></br>+ možnosť zhodnotiť voľné zdroje určené na splátku hypotéky na Hyposporiacom konte'
    }

    this.slsp = {
        "id": 3,
        "banka": "SLSP",
        "urok": 2.20,
        "fixacia": 5,
        "splatka": 260.20,
		"pa": 3146.30,
        "ltv": 100,
        "image": "img/slsp.png",
        "info": '+ podnikatelia môžu získať úver na súkromné účely aj na základe posúdenia ich dosiahnutého percenta z tržieb</br></br>+ pri úveroch nad 70% hodnoty nehnuteľnosti stačí zdokladovať 80% výšky úveru</br></br>+ vybavenie a zaplatenie znaleckého posuku k nehnuteľnosti a vkladu záložného práva do katastra bankou</br></br>+ možnosť splatiť 20% intiny ročne bez poplatku'
    }

	this.sort_urok = [ 0, 1, 2, 3 ];

    this.sort_ltv = [ 1, 2, 0, 3 ];

    this.sort_splatka = [ 0, 1, 2, 3 ];

    this.sort_fixacia = [ 0, 1, 2, 3 ];
    
    this.sort_pa = [ 0, 1, 2, 3 ];

    this.ponuky = [this.unicredit, this.mbank, this.fio, this.slsp];

    this.test = [this.unicredit, this.mbank, this.fio, this.slsp];

}

DB.prototype.getSortedHypo = function(parameter) {
    var sorted = new Array();
    var s;
    switch(parameter) {
        case 'urok': 
            s = this.sort_urok;
            break;
        case 'splatka': 
            s = this.sort_splatka;
            break;
        case 'fixacia': 
            s = this.sort_fixacia;
            break;
        case 'ltv': 
            s = this.sort_ltv;
            break;
        case 'pa':
            s = this.sort_pa;
            break;
        default: 
            s = this.sort_urok;
            break;
    }

    for (var i = 0; i < s.length; i++) {
        sorted.push(this.ponuky[s[i]]);
    };

    return sorted;
};

DB.prototype.getHypoCard = function(h,isFav) {
    var hypo_card = '<div class="hypoteka">';
    hypo_card += '<div class="hypoID" data-value="'+h["id"]+'" style="display:none;"></div>';
    hypo_card += '<div class="hypo-head">';
    hypo_card += '<div class="bankImage"><img src="'+h["image"]+'"></div>';
    hypo_card += '<div class="hypo-buttons">';
    hypo_card += '<a href="poziadaj.html?id='+h["id"]+'"><div class="poziadajButton myButton">Požiadaj</div></a>';
    if (isFav) {
        hypo_card += '<div class="addFavButton myButton circle isFavorite"><img src="img/star-icon.png" width="20" height="20"></div>';
    } else {
        hypo_card += '<div class="addFavButton myButton circle"><img src="img/star-icon.png" width="20" height="20"></div>';
    }
    hypo_card += '<div class="openButton"><div class="arrow_down"></div></div>';
    hypo_card += '</div>';
    hypo_card += '</div><div class="hypo-content"><table>';
    hypo_card += '<tr><td>Úrok</td>';
    hypo_card += '<td>'+h["urok"]+' %</td></tr>';
    hypo_card += '<tr><td>Fixácia</td>';
    if(h["fixacia"] == 1) {
        hypo_card += '<td>'+h["fixacia"]+' rok</td></tr>';
    } else if(h["fixacia"] > 1 && h["fixacia"] < 5) {
        hypo_card += '<td>'+h["fixacia"]+' roky</td></tr>';
    } else {
        hypo_card += '<td>'+h["fixacia"]+' rokov</td></tr>';
    }
    hypo_card += '<tr><td>Mesačná splátka</td>';
    hypo_card += '<td>'+h["splatka"]+' €</td></tr>';
	hypo_card += '<tr><td>P.A.</td>';
    hypo_card += '<td>'+h["pa"]+' €</td></tr>';
    hypo_card += '<tr><td>Max LTV</td>';
    hypo_card += '<td>'+h["ltv"]+' %</td></tr>';
    hypo_card += '</table><div class="hypo-slider">'+h["info"]+'</div></div></div>';
    return hypo_card;
}

DB.prototype.getHypoRow = function(h) {
    var hypo_row = '<tr><td><a class="poziadajLink" href="poziadaj.html?id='+h["id"]+'"><div style="padding:1em 0;"><b>'+h["banka"]+'</b></div></a></td><td>'+h["urok"]+' %</td>';
    if(h["fixacia"] == 1) {
        hypo_row += '<td>'+h["fixacia"]+' rok</td>';
    } else if(h["fixacia"] > 1 && h["fixacia"] < 5) {
        hypo_row += '<td>'+h["fixacia"]+' roky</td>';
    } else {
        hypo_row += '<td>'+h["fixacia"]+' rokov</td>';
    }
    hypo_row += '<td><b>'+h["splatka"]+' €</b></td><td><b>'+h["pa"]+' €</b></td><td>'+h["ltv"]+' %</td>';
    hypo_row += '<td><a href="'+window.location.href+'"><div class="removeFavButton"><img src="img/remove-icon.png" width="25" height="25"></div></a><div class="hypoID" data-value="'+h["id"]+'" style="display:none;"></div></td></tr>';
    return hypo_row;
}

DB.prototype.getHypo = function(id) {
    return this.ponuky[id];
};
