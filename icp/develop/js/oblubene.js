
function getFavCount() {
    var favorites = getFavorite();
    var count = 0;
    if (favorites != null) {
        count = Object.keys(favorites).length;
    }
    return count;
}

function getFavorite() {
    var favorites = $.localStorage.get("fav");
    return favorites;
}


function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function FillOblubene(count) {
        console.log("count: " + count);
        $(".oblubene").find(".fav-count").find("span").html(count);
            $(".oblubene").find('.fav-count').parent().find(".porovnajButton").remove(); 
        if (count > 0) {
            $(".oblubene").find('.fav-count').after('<div class="porovnajButton"><a href="porovnaj.html">Porovnaj</a></div>'); 
        };
    } 